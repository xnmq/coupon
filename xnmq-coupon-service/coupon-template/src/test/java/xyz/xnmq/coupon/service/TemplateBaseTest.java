package xyz.xnmq.coupon.service;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

/**
 * @Created xnmq
 * @Date 2020/7/5
 * @Description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TemplateBaseTest {

    @Autowired
    private TemplateBaseService templateBaseService;

    @Test
    public void findTemplateInfo() throws Exception{
        System.out.println(JSON.toJSONString(templateBaseService.findTemplateInfo(18L)));
        System.out.println(JSON.toJSONString(templateBaseService.findTemplateInfo(2L)));
    }

    @Test
    public void findAllUsableTemplate() throws Exception{
        System.out.println(JSON.toJSONString(templateBaseService.findAllUsableTemplate()));
    }

    @Test
    public void findIdsToTemplateSDK() throws Exception{
        System.out.println(
                JSON.toJSONString(templateBaseService.findIdsToTemplateSDK(Arrays.asList(18L,19L)))
        );
    }
}
