package xyz.xnmq.coupon.service;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;

/**
 * @Created xnmq
 * @Date 2020/6/10
 * @Description
 */
public class Test {
    public static void main(String[] args) {
        LocalDate independenceDay = LocalDate.of(2018, Month.JULY, 4);
        LocalDate christmas = LocalDate.parse("2018-12-25");
        // 174 days
        long until = independenceDay.until(christmas, ChronoUnit.DAYS);

        Period period = independenceDay.until(christmas);
        // 21 days
        int days = period.getDays();

        System.out.println(until + "," + days);

    }
}
