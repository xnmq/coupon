package xyz.xnmq.coupon.service;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.xnmq.coupon.constant.CouponCategory;
import xyz.xnmq.coupon.constant.DistributeTarget;
import xyz.xnmq.coupon.constant.PeriodType;
import xyz.xnmq.coupon.constant.ProductLine;
import xyz.xnmq.coupon.vo.CouponTemplateVO;
import xyz.xnmq.coupon.vo.TemplateRule;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

/**
 * @Created xnmq
 * @Date 2020/7/3
 * @Description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BuildTemplateTest {

    @Autowired
    private BuildTemplateService buildTemplateService;

    /**
     * 测试 构建模板功能
     *
     * @throws Exception
     */
    @Test
    public void testBuildTemplate() throws Exception {
        System.out.println(JSON.toJSONString(buildTemplateService.buildTemplate(fakeTemplateVo())));
        // 因为构建优惠券模板是异步服务， 而Test在执行完，就会关闭服务，导致异步任务也会终止，为了测试，将当前线程休眠5s
        Thread.sleep(5000);


    }

    // 生成 templateVo
    private CouponTemplateVO fakeTemplateVo() {
        CouponTemplateVO vo = new CouponTemplateVO();
        vo.setName("优惠券模板-" + new Date().getTime());
        vo.setLogo("http://www.baidu.com");
        vo.setDetail("这是一张优惠券模板");
        vo.setCategory(CouponCategory.MANJIAN.getCode());
        vo.setProductLine(ProductLine.DABAO.getCode());
        vo.setCount(10);
        vo.setUserId(100L);
        vo.setTarget(DistributeTarget.SINGLE.getCode());

        TemplateRule rule = new TemplateRule();
        rule.setExpiration(new TemplateRule.Expiration(
                PeriodType.SHIFT.getCode(), 1,
                DateUtils.addDays(new Date(), 60).getTime())
        );
        rule.setDiscount(new TemplateRule.Discount(5,1));
        rule.setLimitation(1);
        rule.setUsage(new TemplateRule.Usage(
                "江苏省", "盐城市",
                JSON.toJSONString(Arrays.asList("文娱， “家居")))
        );
        rule.setWeight(JSON.toJSONString(Collections.EMPTY_LIST));

        vo.setRule(rule);
        return vo;
    }
}
