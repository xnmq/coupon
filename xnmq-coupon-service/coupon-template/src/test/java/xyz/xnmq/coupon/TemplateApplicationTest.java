package xyz.xnmq.coupon;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Created xnmq
 * @Date 2020/7/3
 * @Description
 * 模板系统测试程序
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TemplateApplicationTest {
    /**
     * 测试系统是否能正常启动
     */
    @Test
    public void contextLoad(){}
}
