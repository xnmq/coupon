package xyz.xnmq.coupon.schedule;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import xyz.xnmq.coupon.dao.CouponTemplateDao;
import xyz.xnmq.coupon.entity.CouponTemplate;
import xyz.xnmq.coupon.vo.TemplateRule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Created xnmq
 * @Date 2020/6/14
 * @Description
 * 定时任务
 * 定时清理已过期的优惠券模板
 */
@Slf4j
@Component
public class ScheduledTask {
    @Autowired
    private CouponTemplateDao couponTemplateDao;

    /**
     * 定时下线已过期的优惠券模板
     * 每小时执行一次
     */
    @Scheduled(fixedRate = 60 * 60 * 1000)
    public void offlineCouponTemplate(){
        log.info("Start To Expire CouponTemplate.");
        List<CouponTemplate> templates = couponTemplateDao.findAllByExpired(false);
        if(CollectionUtils.isEmpty(templates)){
            log.info("Done To Expire CouponTemplate.");
            return;
        }
        Date cur = new Date();
        // 过期的优惠券模板
        List<CouponTemplate> expiredTemplates = new ArrayList<>(templates.size());

        // 判断优惠券是否过期
        templates.forEach(t -> {
            // 根据优惠券模板规则中的过期时间校验模板是否过期
            TemplateRule rule = t.getRule();
            if(rule.getExpiration().getDeadline() < cur.getTime()){
                t.setExpired(true);
                expiredTemplates.add(t);
            }
        });

        if(CollectionUtils.isNotEmpty(expiredTemplates)){
            log.info("Expired CouponTemplate Num: {}", couponTemplateDao.saveAll(expiredTemplates));
        }

        log.info("Done To Expire CouponTemplate.");

    }
}
