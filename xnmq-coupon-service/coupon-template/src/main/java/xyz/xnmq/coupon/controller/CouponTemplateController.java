package xyz.xnmq.coupon.controller;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.xnmq.coupon.entity.CouponTemplate;
import xyz.xnmq.coupon.exception.CouponCommonException;
import xyz.xnmq.coupon.service.BuildTemplateService;
import xyz.xnmq.coupon.service.TemplateBaseService;
import xyz.xnmq.coupon.vo.CouponTemplateSDK;
import xyz.xnmq.coupon.vo.CouponTemplateVO;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @Created xnmq
 * @Date 2020/6/14
 * @Description
 */
@Slf4j
@RestController
@RequestMapping("template")
public class CouponTemplateController {
    @Autowired
    private BuildTemplateService buildTemplateService;
    @Autowired
    private TemplateBaseService templateBaseService;

    /**
     * 构建 优惠券模板
     * @param vo
     * @return
     * @throws CouponCommonException
     */
    @PostMapping("build")
    public CouponTemplate buildTemplate( @RequestBody CouponTemplateVO vo)
            throws CouponCommonException {
        log.info("Build Template: {}", JSON.toJSONString(vo));
        return buildTemplateService.buildTemplate(vo);
    }

    /**
     * 根据优惠券模板id 获得优惠券模板
     * @param id
     * @return
     * @throws CouponCommonException
     */
    @GetMapping("info")
    public CouponTemplate templateInfo(Long id) throws CouponCommonException{
        log.info("Template Info For: {}", id);
        return templateBaseService.findTemplateInfo(id);
    }

    /**
     * 获得所有可用优惠券模板SDK
     * @return
     * @throws CouponCommonException
     */
    @GetMapping("sdk/all")
    public List<CouponTemplateSDK> findAllUsableTemplateSDK() throws CouponCommonException{
        log.info("Find All Usable Template.");
        return templateBaseService.findAllUsableTemplate();
    }

    /**
     * 获得 模板id 到 CouponTemplateSDK 的映射
     * @param ids
     * @return
     * @throws CouponCommonException
     */
    @GetMapping("sdk/infos")
    public Map<Long, CouponTemplateSDK> findIdsToTemplateSDK(Collection<Long> ids) throws CouponCommonException{
        log.info("Find IdsToTemplateSDK: {}", JSON.toJSONString(ids));
        return templateBaseService.findIdsToTemplateSDK(ids);
    }

}
