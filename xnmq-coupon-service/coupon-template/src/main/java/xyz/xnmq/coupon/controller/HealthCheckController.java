package xyz.xnmq.coupon.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.xnmq.coupon.exception.CouponCommonException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Created xnmq
 * @Date 2020/6/14
 * @Description 健康检查接口
 */
@Slf4j
@RestController
public class HealthCheckController {

    /**
     * 当前微服务会注册进Eureka
     * 而可以通过DiscoveryClient 和 Registration 可以获得Eureka的信息
     * <p>
     * 注意1：Registration在idea中会报错，但是这个是idea的问题，并不影响
     * 注意2：在服务注册进Eureka后，大约需要2min，这两个参数才能获得Eureka的信息
     * 注意3：DiscoveryClient 和 Registration 有很多包提供，正确的包是
     * org.springframework.cloud.client.discovery.DiscoveryClient;
     * org.springframework.cloud.client.serviceregistry.Registration;
     */
    @Autowired
    private DiscoveryClient client;
    @Autowired
    private Registration registration;

    /**
     * 健康检查接口
     * 判断模板服务是否启动
     * 127.0.0.1:7001/coupon-template/health
     * 通过网关访问的地址： 127.0.0.1:9000/coupon/coupon-template/health
     * @return
     */
    @GetMapping("/health")
    public String health() {
        log.debug("view health api");
        return "CouponTemplate Is OK!";
    }

    /**
     * 统一异常检查接口
     * 判断统一异常是否可以使用
     * 说实话，这个接口没有太明白是什么作用，为什么要特意检查统一异常是否可用呢？
     *
     * @return
     * @throws CouponCommonException
     */
    @GetMapping("/exception")
    public String exception() throws CouponCommonException {
        log.debug("view exception api");
        throw new CouponCommonException("CouponTemplate Has Some Problem");
    }

    /**
     * 获得 Eureka Server 上的微服务元信息
     *
     * @return
     */
    @GetMapping("/eurekaInfo")
    public List<Map<String, Object>> eurekaInfo() {
        /**
         * 服务id（serviceId） 和 实例id（instanceId）的区别
         * 因为可能会部署多个实例，而这些实例在Eureka中是使用一个服务id的
         */
        List<ServiceInstance> instances = client.getInstances(registration.getServiceId());
        List<Map<String, Object>> result = new ArrayList<>(instances.size());
        instances.forEach(i -> {
            Map<String, Object> info = new HashMap<>();
            info.put("serviceId", i.getServiceId());
            info.put("instanceId", i.getInstanceId());
            info.put("port", i.getPort());
            result.add(info);
        });
        return result;
    }
}
