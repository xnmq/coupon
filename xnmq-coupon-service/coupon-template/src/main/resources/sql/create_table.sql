-- 创建表
create table if not exists `coupon-dev`.`coupon_template`
(
  `id`           bigint(20)   not null auto_increment,
  `available`    boolean               default false comment '是否是可用状态 true 可用 false 不可用',
  `expired`      boolean               default false comment '是否过期 true 是 false 否',
  `name`         varchar(64)           default '' comment '优惠券名称',
  `logo`         varchar(255)          default '' comment '优惠券logo',
  `detail`    VARCHAR(255)          default '' comment '优惠券描述',
  `category`     varchar(64)           default '' comment '优惠券分类',
  `product_line` int(11)               default '0' comment '产品线',
  `coupon_count` int(11)               default '0' comment '总数',
  `create_time`  datetime              default '0000-01-01 00:00:00' comment '创建时间',
  `user_id`      bigint(20)            default '0' comment '创建用户',
  `template_key` varchar(255) not null default '' comment '优惠券模板编码',
  `target`       int(11)               default '0' comment '目标用户',
  `rule`         varchar(1024)         default '' comment '优惠券规则 TemplateRule 的 json表示',

  primary key (`id`),
  key `idx_category` (`category`),
  key `idx_user_id` (`user_id`),
  unique key `name` (`name`)
) engine = InnoDB
  auto_increment = 10
  default charset = utf8 comment ='优惠券模板表';

-- 清空表数据
truncate coupon_template;

